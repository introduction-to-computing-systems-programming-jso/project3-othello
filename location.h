#ifndef LOCATION_ICSP_HEADER
#define LOCATION_ICSP_HEADER

#define BBlack_FWhite 7 /*backeground:black, forground:white*/
#define BBlack_FGreen 10 /*backeground:black, forground:green*/
#define BBlack_FRed 12 /*backeground:black, forground:red*/
#define BBlack_FPurple 13 /*backeground:black, forground:purple*/
#define BRed_FWhite 207 /*backeground:red, forground:white*/
#define BWhite_FBlack 240 /*backeground:white, forground:black*/
#define BGray_FBlack 112 /*backeground:Gray, forground:black*/
#define BWhite_FBlue 249 /*backeground:white, forground:blue*/
#define BWhite_FRed 252 /*backeground:white, forground:red*/
#define BWhite_FGray 248 /*backeground:white, forground:gray*/
#define GREEN 163
#define WHITE 255
#define BLUE 187


void init_console_functions (void); // in order to use functions of this file you must call this function in first line of main
void gotoxy (int x, int y); //move cursor of console to (x,y), after calling this function you can use printf to print at (x,y)
void clrscr(void); // clear console
void set_color(int color); //used above defined colors as function input in order to make your project colorfull

#endif
